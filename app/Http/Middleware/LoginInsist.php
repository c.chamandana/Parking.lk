<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;

class LoginInsist
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() == false)
        {   
            Session::flash("message", "Please log in to continue");
            Session::flash("message-type", "danger");

            return redirect("/login");
        }

        return $next($request);
    }
}
