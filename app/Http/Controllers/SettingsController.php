<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;

class SettingsController extends Controller
{
	public function basic(Request $request)
	{
		$newname = $request->name;

		if (isset($newname) && Auth::check())
		{
			$user = Auth::user();
			$user->name = $newname;
			$user->save();

			Session::flash("message", "Name changed successfully");
			Session::flash("message-type", "success");

			return redirect("/dashboard#settings");
		} 
		else 
		{
			Session::flash("message", "Wrong request type");
			Session::flash("message-type", "danger");

			return redirect("/dashboard#settings");
		}
	}

	public function changePassword(Request $request){
		if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
		// The passwords matches
			Session::flash("message", "Current password you entered is wrong");
			Session::flash("message-type", "danger");

			return redirect("/dashboard#settings");
		}
		if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
		//Current password and new password are same
			Session::flash("message", "You cannot use the previous password again, please try another one");
			Session::flash("message-type", "danger");

			return redirect("/dashboard#settings");
		}
		$validatedData = $request->validate([
			'current-password' => 'required',
			'new-password' => 'required|string|min:6|confirmed',
		]);

	//Change Password
		$user = Auth::user();
		$user->password = bcrypt($request->get('new-password'));
		$user->save();

		Session::flash("message", "Your password has been saved successfully");
		Session::flash("message-type", "success");

		return redirect("/dashboard#settings");
	}

	public function deleteAccount(Request $request)
	{
		$user = Auth::user();

		if ($user)
		{
			$user->delete();

			Session::flash("message", "Your account has been deleted successfully");
			Session::flash("message-type", "success");

			return redirect("/");
		} else {
			Session::flash("message", "You must be logged in to continue");
			Session::flash("message-type", "danger");

			return redirect("/login");
		}
	}

	public function switchAccount(Request $request)
	{
		$user = Auth::user();

		if ($user)
		{
			if ($user->type == 0)
			{
				$user->type = 1; // Make the user a 
			} 
			else if ($user->type == 1)
			{
				$user->type = 0; // Make the user a driver
			}

			$user->save();

			Session::flash("message", "Your account has been deleted successfully");
			Session::flash("message-type", "success");

			return redirect("/dashboard#settings");
		} else {
			Session::flash("message", "You must be logged in to continue");
			Session::flash("message-type", "danger");

			return redirect("/login");
		}
	}

}
