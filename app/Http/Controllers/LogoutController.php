<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class LogoutController extends Controller
{
    public function logout(Request $request)
    {
    	Auth::logout(); // Logout the user

    	return redirect("/");
    }
}
