<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Spot;
use Session;

class SpotController extends Controller
{
    public function create(Request $request)
    {
    	if (Auth::check())
    	{
		   	$owner = Auth::user()->email;

		   	if ($owner !== null)
		   	{
		   		// Create a new spot
		   		$spot = new Spot;
		   		$spot->owner = $owner;
		   		$spot->address1 = $request->address1;
		   		$spot->address2 = $request->address2;
		   		$spot->address3 = $request->address3;

		   		// Save the spot
		   		$spot->save();

		   		Session::flash("message-type", "success");
		    	Session::flash("message", "Your Parking Spot has been added to the map");

		    	return redirect("/dashboard");
		   	} 
		   	else
		   	{
		   		Session::flash("message-type", "danger");
		    	Session::flash("message", "Could not resolve the user");

		    	return redirect("/login");
		   	}
		}
    }

    public function save(Request $request, $spotid)
    {
    	$user = Auth::user()->email;

    	if ($user !== null)
    	{
    		$spot = Spot::where("id", $spotid);

    		if ($spot->exists())
    		{
    			$spot = $spot->first();

	    		$spot->address1 = $request->address1;
	    		$spot->address2 = $request->address2;
	    		$spot->address3 = $request->address3;

	    		$spot->save();

	    		Session::flash("message-type", "success");
		    	Session::flash("message", "The Park details successfully saved");

		    	return redirect("/dashboard");
	    	}else{
	    		Session::flash("message-type", "danger");
		    	Session::flash("message", "Could not resolve the Parking Lot");

		    	return redirect("/dashboard");
	    	}
    	}
    	else
   	 	{
    		Session::flash("message-type", "danger");
	    	Session::flash("message", "Could not resolve the user");

	    	return redirect("/login");
    	}
    }
}
