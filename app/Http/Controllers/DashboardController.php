<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function view(Request $request)
    {
    	return view ("dashboard");
    }

    public function loadTab(Request $request, $tab)
    {
    	return view ("dashboard." . $tab);
    }
}
