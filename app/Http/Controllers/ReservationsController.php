<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Reservation;
use Session;

class ReservationsController extends Controller
{
    public function accept(Request $request, $reservationid)
    {
        $user = Auth::user();

        if (Auth::check())
        {
            $reservation = Reservation::where('id', $reservationid)->first();
            
            if ($reservation)
            {
                // If reservation exists
                $spot = \App\Spot::where("id", $reservation->spotid)->first(); // get the spot

                if ($spot->owner == $user->email)
                {
                    // Approve the reservation
                    $reservation->approved = 1;
                    $reservation->save();

                    Session::flash("message-type", "success");
                    Session::flash("message", "Parking Reservation Request successfully accepted");

                    return redirect("/dashboard#requests");
                }else{
                    Session::flash("message-type", "danger");
                    Session::flash("message", "You do not own the Parking Spot");

                    return redirect("/dashboard");
                }
            }
        }else{
            Session::flash("message-type", "danger");
            Session::flash("message", "Please login to continue");

            return redirect("/login");
        }
    }

    public function reserve(Request $request)
    {
    	$spotid = $request->spotid;
    	$user = Auth::user();
    	
    	$starttime_str = $request->starttime;
    	$endtime_str = $request->endtime;

    	// If spot is reservable
    	if (AJAXController::checkSpotStatus($spotid))
    	{
	    	// Put the request in the database
	    	$reservation = new Reservation;
	    	$reservation->spotid = $spotid;
			$reservation->driver = $user->email;
			$reservation->approved = 0;
	    	$reservation->start_time = $starttime_str;
	    	$reservation->end_time = $endtime_str;

	    	// Save the reservation
	    	$reservation->save();

			Session::flash("message-type", "success");
	    	Session::flash("message", "Parking spot successfully reserved");

	    	return redirect("/dashboard#myreservations");
	    } else {
	    	Session::flash("message-type", "error");
	    	Session::flash("message", "The parking spot is already reserved");

	    	return redirect("/dashboard");
	    }
    }

    public function cancel(Request $request, $reservationid)
    {
    	// Select the reservation
    	$reservation = Reservation::where("id", $reservationid);

    	if ($reservation->exists())
    	{
    		$reservation = $reservation->first(); // Select the first one
    		$reservation->delete();

    		Session::flash("message-type", "success");
	    	Session::flash("message", "Reservation successfully canceled");

	    	return redirect("/dashboard");
    	}else {
    		Session::flash("message-type", "success");
	    	Session::flash("message", "Reservation does not exist please retry");

	    	return redirect("/dashboard");
    	}
    }
}
