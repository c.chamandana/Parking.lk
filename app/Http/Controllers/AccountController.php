<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Session;

class AccountController extends Controller
{
    public function delete(Request $request, $userid)
    {
    	$deletingUser = User::where("id", $userid);
    	$adminUser = Auth::user();

        // Only if the request is from a admin
    	if ($adminUser->type == 2)
    	{
    		// Delete the user from the database
    		$deletingUser->delete(); 

    		Session::flash("message", "Account deleted successfully");
    		Session::flash("message-type", "success");

    		return redirect("/dashboard");
    	} else {
    		Session::flash("message", "You are not an Administrator to delete other accounts");
    		Session::flash("message-type", "danger");

    		return redirect("/login");
    	}
    }
}
