<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Spot;
use App\Reservation;

class AJAXController extends Controller
{
    public function getSpotDetails(Request $request, $spotid)
    {
        $spot = Spot::where("id", $spotid)->first();

        return json_encode([
            "address" => $spot->address1 . ", " . $spot->address2 . ", " . $spot->address3,
            "address1" => $spot->address1,
            "address2" => $spot->address2,
            "address3" => $spot->address3,
            "owner" => $spot->owner,
            "status" => AJAXController::checkSpotStatus($spotid)
        ]);
    }

    public function spotSearch(Request $request, $query = "")
    {
    	$allspots = Spot::all(); // Get all the spots
    	$returnobject = [];

    	foreach ($allspots as $spot) {
    		$status = AJAXController::checkSpotStatus($spot->id);
    		$found = false;

    		if ($query == "")
    			$found = true;
    		else
				$found = (AJAXController::strContains($spot->address1, $query) || AJAXController::strContains($spot->address2, $query) || AJAXController::strContains($spot->address3, $query) || AJAXController::strContains($spot->owner, $query));


    		if ($found)
    		{
	    		$returnobject[] = [
	    			"address" => $spot->address1 . ",\n" . $spot->address2 . ",\n" . $spot->address3 . ".",
	    			"statusLower" => strtolower($status),
	    			"status" => strtoupper($status),
                    "spotid" => $spot->id
	    		];
	    	}
    	}

    	return json_encode([
    		"results" => $returnobject
    	]);
    }

    public static function strContains($haystack, $needle)
    {
    	return (strpos(strtolower($haystack), strtolower($needle)) !== false);
    }

    public static function checkSpotStatus($spotid)
    {
    	$datetime_format = "Y-m-d H:i:s";
    	date_default_timezone_set('Asia/Colombo');

    	// Get all approved reservations
    	$reservations = Reservation::where("spotid", $spotid)->where("approved", 1)->get();

    	if (sizeof($reservations) == 0)
    	{
    		return "available";
    	} else {
	    	foreach ($reservations as $reservation)
	    	{
	    		$start = $reservation->start_time;
	    		$end = $reservation->end_time;

	    		$start_date = date_create_from_format($datetime_format, $start);
	    		$end_date = date_create_from_format($datetime_format, $end);

	    		$current_date = date_create_from_format($datetime_format, date($datetime_format, time()));

	    		if (($start_date < $current_date) && ($end_date > $current_date))
	    		{
	    			// You're in the reservation time
	    			return "reserved";
	    		} else {
	    			return "available";
	    		}
	    	}
	    }

    }
}
