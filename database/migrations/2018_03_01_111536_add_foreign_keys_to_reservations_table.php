<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReservationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reservations', function(Blueprint $table)
		{
			$table->foreign('driver', 'driver_user_fk')->references('email')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('spotid', 'spot_fk')->references('id')->on('spots')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reservations', function(Blueprint $table)
		{
			$table->dropForeign('driver_user_fk');
			$table->dropForeign('spot_fk');
		});
	}

}
