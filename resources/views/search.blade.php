@extends ("layouts.home")

@section ("content")

<?php 
	
	function strcontains($a, $b)
	{
		if (strpos($a, $b) !== false) {
		   return true;
		}
	}

	$found = false;

?>

@foreach (\App\Spot::all() as $spot)
	@if (strcontains($spot->address1 .", " . $spot->address2 . ", " . $spot->address3, $_GET["address"]))
		<?php $found = true; ?>
	@endif
@endforeach

	<!-- Navigation bar -->

	@include ("partials.navigation")

	<div style="background:white; min-height:100vh;">
		<div class="container" style="padding-top:120px;padding-bottom:100px">
			<a href="/"><i class="fa fa-chevron-left"></i> Back to search</a>
			<h3>Search Results for &quot;<?php echo $_GET["address"]; ?>&quot;</h3>


			@if ($found)
				<table class="table table-striped" style="margin-top:50px; width:100%">
					<thead style="color:white !important">
						<th>Availability</th>
						<th>Address</th>
						<th style="text-align:center">Actions</th>
					</thead>
					@foreach (\App\Spot::all() as $spot)
						@if (strcontains($spot->address1 .", " . $spot->address2 . ", " . $spot->address3, $_GET["address"]))
							<?php $found = true; ?>
							<tr>
								@if (\App\Http\Controllers\AJAXController::checkSpotStatus($spot->id) == "available")
									<td style="color:salmon; width:150px"><i class="fa fa-check"></i> Available</td>
								@else
									<td style="color:gray; width:150px"><i class="fa fa-times"></i> Reserved</td>
								@endif
								<td>{{ $spot->address1 .", " . $spot->address2 . ", " . $spot->address3 }}</td>
								<td style="width:150px;text-align:center">
									<a href="/login"><button class="btn btn-primary">Reserve</button></a>
								</td>
							</tr>
						@endif
					@endforeach

				</table>
			@else
				<div class="padded" style="margin-top:50px;">
					No results found for your query, please retry
				</div>			
			@endif

			<h3 style="margin-top:60px">Map of Search Results</h3>
			<div id="map" style="height: 600px; width: 100%;margin-top:25px;">
			</div>

		</div>
	</div>

		
	@include("partials.footer")
@stop

@section ("scripts")
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
	<script type="text/javascript">
		var locations = [
		<?php $myspots = \App\Spot::all(); ?>

		@if (sizeof($myspots) > 0)
		@foreach ($myspots as $spot)
		<?php

		$status =\App\Http\Controllers\AJAXController::checkSpotStatus($spot->id);
		$fileinfo = "red_location.png";

		if ($status == "available")
		{
			$fileinfo = "red_location.png";
		}else if ($status == "reserved")
		{
			$fileinfo = "gray_location.png";
		}

		?>
		{ address: "{{ $spot->address1 . ", " . $spot->address2 . ", " . $spot->address3}}", status: "{{ $fileinfo }}" },
		@endforeach
		@endif
		];

		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 15,
			center: new google.maps.LatLng(6.92929,79.859227),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});

		var geocoder = new google.maps.Geocoder();

		for (var i = 0; i < locations.length; i++)
		{
			geocodeAddress(geocoder, map, locations[i]);
		}

		function geocodeAddress(geocoder, resultsMap, spot) {
			geocoder.geocode({'address': spot["address"]}, function(results, status) {
				if (status === 'OK') {
					
					resultsMap.setCenter(results[0].geometry.location);

					var marker = new google.maps.Marker({
						map: resultsMap,
						icon: "/images/" + spot["status"],
						position: results[0].geometry.location
					});

				} else {
					console.log("Location could not be found")
				}
			});
		}

	</script>
@stop
