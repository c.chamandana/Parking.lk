<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Parking.lk</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />

	<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/paper-kit.css?v=2.1.0" rel="stylesheet"/>
	<link href="assets/css/demo.css" rel="stylesheet" />

	<!--     Fonts and icons     -->
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css'>
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/css/nucleo-icons.css" rel="stylesheet" />

</head>
<body>

    <!--    navbar come here          -->

    <nav class="navbar navbar-expand-md fixed-top " color-on-scroll="500">
      <div class="container">
        <div class="navbar-translate">
           <button class="navbar-toggler navbar-toggler-right navbar-burger" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-bar"></span>
              <span class="navbar-toggler-bar"></span>
              <span class="navbar-toggler-bar"></span>
          </button>
          <a class="navbar-brand" href="https://www.creative-tim.com">Parking.lk</a>
      </div>
      <div class="collapse navbar-collapse" id="navbarToggler">
       <ul class="navbar-nav ml-auto">
          <li class="nav-item">
           <a href="documentation/tutorial-components.html" target="_blank" class="nav-link"><i class="nc-icon nc-alert-circle-i"></i> About Us</a>
       </li>			
       <li class="nav-item">
         <a href="documentation/tutorial-components.html" target="_blank" class="nav-link"><i class="nc-icon nc-email-85"></i> Contact Us</a>
     </li>
     <li class="nav-item">
         <a href="documentation/tutorial-components.html" target="_blank" class="nav-link"><i class="nc-icon nc-circle-10"></i> Signup</a>
     </li>
     <li class="nav-item">
        <div class="dropdown">
          <a href="#" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
             login
             <b class="caret"></b>
         </a>
         <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
             <li class="dropdown-header">Select</li>
             <a class="dropdown-item" href="#pk">Driver</a>
             <a class="dropdown-item" href="#pk">Parking Owner</a>
             <a class="dropdown-item" href="#pk">Admin</a>
             <div class="dropdown-divider"></div>
             <a class="dropdown-item" href="#pk">Logout</a>
         </ul>
     </div>
 </li>
</ul>
</div>
</div>
</nav>

<!-- end navbar  -->




<!-- login search starts here  -->





<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="tim-title">
                    <h3>Parks found</h3>
                </div>
                <div class="park-list-container">


                    <!-- Park list card -->
                    <div class="modal-content park-list">
                        <div class="modal-header">
                            <h5 class="modal-title text-center" id="exampleModalLabel">Colombo Fort</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            The description of the park goes here
                        </div>
                        <br>
                        <div class="modal-footer">
                            <div class="left-side">
                                <h5 class="price-per-hour">Rs 100</h5>
                            </div>
                            <div class="divider"></div>
                            <div class="right-side">
                                <button type="button" class="btn btn-danger btn-link">Request</button>
                            </div>
                        </div>
                    </div>
                    <!-- Park list card end -->
                    <div class="modal-content park-list">
                        <div class="modal-header">
                            <h5 class="modal-title text-center" id="exampleModalLabel">Colombo galle face</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            The description of the park goes here
                        </div>
                        <br>
                        <div class="modal-footer">
                            <div class="left-side">
                                <h5 class="price-per-hour">Rs 250</h5>
                            </div>
                            <div class="divider"></div>
                            <div class="right-side">
                                <button type="button" class="btn btn-danger btn-link">Request</button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-content park-list">
                        <div class="modal-header">
                            <h5 class="modal-title text-center" id="exampleModalLabel">Colombo galle face</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            The description of the park goes here
                        </div>
                        <br>
                        <div class="modal-footer">
                            <div class="left-side">
                                <h5 class="price-per-hour">Rs 250</h5>
                            </div>
                            <div class="divider"></div>
                            <div class="right-side">
                                <button type="button" class="btn btn-danger btn-link">Request</button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-content park-list">
                        <div class="modal-header">
                            <h5 class="modal-title text-center" id="exampleModalLabel">Colombo galle face</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            The description of the park goes here
                        </div>
                        <br>
                        <div class="modal-footer">
                            <div class="left-side">
                                <h5 class="price-per-hour">Rs 250</h5>
                            </div>
                            <div class="divider"></div>
                            <div class="right-side">
                                <button type="button" class="btn btn-danger btn-link">Request</button>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>

            <div class="col-md-6">
                <div class="row google-map">
                    <div id="googleMap" style="width:100%;height:800px;"></div>

                    <script>
                        function myMap() {
                            var mapProp= {
                                center:new google.maps.LatLng(51.508742,-0.120850),
                                zoom:5,
                            };
                            var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
                        }
                    </script>     
                </div>                  
                
            </div>
        </div>
    </div>

    <!-- location map -->


    
</div>

</div>


</div>
<br>

</div>
</div>


<!-- Modal Bodies come here -->

<!--   end modal -->

<!-- Footer Starts here -->

<footer class="footer">
	<div class="container">
		<div class="row">
			<nav class="footer-nav">
				<ul>
					<li><a href="http://www.creative-tim.com">Parking.lk</a></li>
					<li><a href="http://blog.creative-tim.com">Newsletter</a></li>
					<li><a href="http://www.creative-tim.com/license">Licenses</a></li>
				</ul>
			</nav>
			<div class="credits ml-auto">
				<span class="copyright">
					© <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by www.nishdlive.com
				</span>
			</div>
		</div>
	</div>
</footer>

<!-- Footer Ends here -->
</body>
<!-- Core JS Files -->
<script src="assets/js/jquery-3.2.1.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui-1.12.1.custom.min.js" type="text/javascript"></script>
<script src="assets/js/popper.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!-- Switches -->
<script src="assets/js/bootstrap-switch.min.js"></script>

<!--  Plugins for Slider -->
<script src="assets/js/nouislider.js"></script>

<!--  Plugins for DateTimePicker -->
<script src="assets/js/moment.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>

<!--  Paper Kit Initialization snd functons -->
<script src="assets/js/paper-kit.js?v=2.1.0"></script>

<!-- google maps -->
<script src="https://maps.googleapis.com/maps/api/js?callback=myMap"></script>
</html>
