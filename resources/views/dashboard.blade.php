@extends ("layouts.dashboard")

@section ("content")

<div class="user-information-card" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	<table>
		<tr>
			<td>
				<img class="profile-picture" src="/images/user.jpg">
			</td>
			<td>
				<div class="profile-information">
					<span class="user-name">{{ Auth::user()->name }}</span>
					<span class="user-email">{{ Auth::user()->email }}</span>
				</div>
			</td>
			<td>
				<i class="fa fa-chevron-down"></i>
			</td>
		</tr>		
	</table>

</div>

<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
	<a class="dropdown-item" href="/logout">Log Out</a>
	<div class="dropdown-divider"></div>
	<a class="dropdown-item" href="/dashboard#settings">Profile Settings</a>
	<a class="dropdown-item" href="/dashboard#settings">Account Settings</a>
</div>

<div class="row dashboard-row">
	<div class="aside-bar col col-xl-2 col-lg-3 col-md-4 hide-on-sm">
		<div class="brand-name">
			<a class="brand-link" href="/"><i class="fa fa-chevron-left"></i> Parking.lk</a>
		</div>

		<ul class="dashboard-links">
			@if(Auth::user()->type == 1)
			{{-- PARKING SPOT OWNER --}}
			<a href="/" class="a"><li><i class="fa fa-home"></i> GO TO HOME</li></a>
			<a href="#myspots" class="a"><li><i class="fa fa-map-marker"></i> MY PARKING SPOTS</li></a>
			<a href="#requests" class="a"><li><i class="fa fa-book"></i> REQUESTS</li></a>
			<a href="#settings" class="a"><li><i class="fa fa-cog"></i> SETTINGS</li></a>
			@elseif(Auth::user()->type == 0) 
			{{-- DRIVER LINKS --}}
			<a href="/" class="a"><li><i class="fa fa-home"></i> GO TO HOME</li></a>
			<a href="#spots" class="a"><li><i class="fa fa-map-marker"></i> PARKING SPOTS</li></a>
			<a href="#myreservations" class="a"><li><i class="fa fa-book"></i> MY RESERVATIONS</li></a>
			<a href="#settings" class="a"><li><i class="fa fa-cog"></i> SETTINGS</li></a>
			@elseif(Auth::user()->type == 2)
			{{-- A SUPER ADMIN --}}
			<a href="/" class="a"><li><i class="fa fa-home"></i> GO TO HOME</li></a>
			<a href="#overview" class="a"><li><i class="fa fa-bar-chart"></i> OVERVIEW</li></a>
			<a href="#users" class="a"><li><i class="fa fa-user"></i> USER ACCOUNTS</li></a>
			<a href="#settings" class="a"><li><i class="fa fa-cog"></i> SETTINGS</li></a>
			@endif
		</ul>
	</div>

	<div id="display" class="content-bar col col-xl-10 col-lg-9 col-md-8">
		{{-- Do not write anything here automatically loaded by the router --}}
	</div>
</div>

@endsection

@section("scripts")
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script type="text/javascript">

	Handlebars.registerHelper('breaklines', function(text) {
		text = Handlebars.Utils.escapeExpression(text);
		text = text.replace(/(\r\n|\n|\r)/gm, '<br>');
		return new Handlebars.SafeString(text);
	});

	@if (Auth::user()->type == 1)
		// Spot Owner
		var _default_route = "myspots";
		@elseif (Auth::user()->type == 0)
		// Driver
		var _default_route = "spots";
		@elseif(Auth::user()->type == 2)
		var _default_route = "overview";
		@endif

		// Occurs when the page is refreshed
		function refreshPages(route)
		{
			switch(route)
			{
				case "/dashboard/tab/overview":

				var ctx = $("#user-accounts-chart");
				ctx.height = 500;

				var myChart = new Chart(ctx, {
					type: 'pie',
					data: {
						labels: ["Drivers", "Owners"],
						datasets: [{
							label: '# of Votes',
							data: [{{ \App\User::where("type", 0)->count() }}, {{ \App\User::where("type", 1)->count() }}],
							backgroundColor: [
								'rgba(255, 99, 132, 0.2)',
								'rgba(54, 162, 235, 0.2)',
							],
							borderWidth: 1
						}]
					},
					options: {
						maintainAspectRatio: false
					}
				});

				<?php 

				date_default_timezone_set("Asia/Colombo"); // Set the default timezone first

				$users = \App\User::all(); // Get all the users

				$days = [];

				foreach ($users as $user)
				{
					if ($user->created_at->year == date("Y") && $user->created_at->month == date("m"))
					{
						if (isset($days[$user->created_at->day]))
							$days[$user->created_at->day] = $days[$user->created_at->day] + 1;
						else 
							$days[$user->created_at->day] = 1;
					}
				}

				?>

				var ctx2 = $("#month-accounts");
				ctx2.height = 500;

				var myChart2 = new Chart(ctx2, {
					type : "bar",
					data: {
				      labels: [
				      	@for ($i = 1; $i <= cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y")) ; $i++)
				      		"{{ $i }}",
				      	@endfor
				      ],
				      datasets: [
				        {
				          label: "Population (millions)",
				          backgroundColor: [
				          	@for ($i = 1; $i <= cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y")) ; $i++)
				      			"salmon",
				      		@endfor
				          ],
				          data: [
				          	@for ($i = 1; $i <= cal_days_in_month(CAL_GREGORIAN, date("m"), date("Y")) ; $i++)
					      		@if (isset($days[$i]))
					      			{{ $days[$i] }},
					      		@else
					      			0,
					      		@endif
					      	@endfor
				          ]
				        }
				      ]
				    },
					options: {
						maintainAspectRatio : false,
						 scales: {
					         yAxes: [{
					             ticks: {
					                 beginAtZero: true,
					                 userCallback: function(label, index, labels) {
					                     // when the floored value is the same as the value we have a whole number
					                     if (Math.floor(label) === label) {
					                         return label;
					                     }

					                 },
					             }
					         }],
					     },
					}
				})

				var locations = [
				<?php $myspots = \App\Spot::all(); ?>

				@if (sizeof($myspots) > 0)
				@foreach ($myspots as $spot)
				<?php

				$status =\App\Http\Controllers\AJAXController::checkSpotStatus($spot->id);
				$fileinfo = "red_location.png";

				if ($status == "available")
				{
					$fileinfo = "red_location.png";
				}else if ($status == "reserved")
				{
					$fileinfo = "gray_location.png";
				}

				?>
				{ address: "{{ $spot->address1 . ", " . $spot->address2 . ", " . $spot->address3}}", status: "{{ $fileinfo }}" },
				@endforeach
				@endif
				];

				var map = new google.maps.Map(document.getElementById('map'), {
					zoom: 15,
					center: new google.maps.LatLng(6.92929,79.859227),
					mapTypeId: google.maps.MapTypeId.ROADMAP
				});

				var geocoder = new google.maps.Geocoder();

				for (var i = 0; i < locations.length; i++)
				{
					geocodeAddress(geocoder, map, locations[i]);
				}


				break;
				case "/dashboard/tab/users":
					$('#drivers-table').DataTable();
					$('#owners-table').DataTable();

				break;
				case "/dashboard/tab/requests":
				break;
				case "/dashboard/tab/myspots":
				var locations = [
				<?php $myspots = \App\Spot::where("owner", \Auth::user()->email)->get(); ?>

				@if (sizeof($myspots) > 0)
				@foreach ($myspots as $spot)
				<?php

				$status =\App\Http\Controllers\AJAXController::checkSpotStatus($spot->id);
				$fileinfo = "red_location.png";

				if ($status == "available")
				{
					$fileinfo = "red_location.png";
				}else if ($status == "reserved")
				{
					$fileinfo = "gray_location.png";
				}

				?>
				{ address: "{{ $spot->address1 . ", " . $spot->address2 . ", " . $spot->address3}}", status: "{{ $fileinfo }}" },
				@endforeach
				@endif
				];

				var map = new google.maps.Map(document.getElementById('map'), {
					zoom: 15,
					center: new google.maps.LatLng(6.92929,79.859227),
					mapTypeId: google.maps.MapTypeId.ROADMAP
				});

				var geocoder = new google.maps.Geocoder();

				for (var i = 0; i < locations.length; i++)
				{
					geocodeAddress(geocoder, map, locations[i]);
				}

					//Add the triggers for spot click triggers and empty clickers
					$(".spot-click-trigger").click(function(e)
					{
						var spotid = $(this).data("spotid");

						$.ajax({
							url : "/ajax/spot-details/" + spotid,
							method : "GET",
							success : function(data)
							{
								data = JSON.parse(data);

								// Show the address lines in the form
								$("#edit-address1").val(data.address1);
								$("#edit-address2").val(data.address2);
								$("#edit-address3").val(data.address3);	

								// Set the spot id
								$("#edit-spot-form").attr("action", "/save/spot/" + spotid);

								// Show the modal
								$("#edit-modal").modal();
							},
							error : function(er)
							{
								console.log(er)
							}
						});

						e.preventDefault();
					});

					$("#add-spot-trigger").click(function()
					{
						// Empty out the addresses in the modal
						$("#add-address1").val("");
						$("#add-address2").val("");
						$("#add-address3").val("");

						$("#add-modal").modal();
					});



					break;
					case "/dashboard/tab/spots":
					reloadQuery();

					$("#search_query_form").submit(function(e)
					{
							reloadQuery(); // Reload the content
							e.preventDefault();
						});

					$("#starttime").bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD HH:mm:ss' });
					$("#endtime").bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD HH:mm:ss' });

					break;

					case "/dashboard/tab/myreservations":
					$(".cancel-reserve-button").click(function()
					{
						var href = $(this).data("href"); // Get the href;
						$("#cancel-yes-button").attr("href", href); // Set the hyper reference
					});
					break;
				}
			}

			function updateCost()
			{
				var starttime = $("#starttime").val();
				var endtime = $("#endtime").val();

				if (starttime.replace(" ", "").length > 0 && endtime.replace(" ", "").length > 0)
				{
					var format = "YYYY-MM-DD HH-mm-ss";

					var start = moment(starttime, format);
					var end = moment(endtime, format);

					var duration = moment.duration(end.diff(start));
					var hours = duration.asHours();

					var cost = parseInt(100 * hours);

					$("#estimated-cost").val("LKR. " + cost.toString() + ".00");
				}
			}

			function reloadQuery()
			{
				$("#empty_search_results").hide();

			// Empty out the results container
			document.getElementById("search-result-container").innerHTML = "";
			
			var url = "/ajax/spots";

			if ($("#search_query_txt").val().replace(" ", "") != "")
				url = "/ajax/spots/" + $("#search_query_txt").val();

			$("#search-result-container").fadeOut();

			$.ajax({
				url : url,
				type : "GET",
				success : function(data)
				{
					data = JSON.parse(data);

					if (data["results"].length > 0)
					{
						var source =  document.getElementById("spot-template").innerHTML;;
						var template = Handlebars.compile(source);

						for(var i = 0; i < data["results"].length; i++)
						{
							var html = template(data.results[i]);
							$("#search-result-container").append(html); // Add the card
						}

						$("#search-result-container").fadeIn();


						$(".spot-modal-trigger").click(function()
						{				
							var spotid = $(this).data("spotid");

							$.ajax({
								url : "/ajax/spot-details/" + spotid,
								type: "get",
								success: function(data)
								{
									var data = JSON.parse(data); // Parse the JSON data to object

									if (data.status == "available")
									{
										$("#spotid").val(spotid);
										$("#address").val(data.address);
										$("#owner").val(data.owner);

										// Load the google maps
										$("#google-maps-frame").attr("src", "https://www.google.com/maps/embed/v1/directions?key=AIzaSyBgMF3OEBQibKRci4DIyLuur6Fc73rIPJw&origin=Colombo&destination=" + encodeURI(data.address) +"&avoid=tolls|highways");

										$("#reserve-modal").modal();

										$("#request-reserve-btn").click(function()
										{
											if($("#starttime").val().replace(" ", "").length == 0 || 
												$("#endtime").val().replace(" ", "").length == 0)
											{
												$.notify({
													// options
													message: 'Please enter the arrival and departure time to request reservation' 
												},{
													// settings
													type: 'danger'
												});
											}else {
												$("#reserve-spot-form").submit(); // Submit the form
											}
										});

										$("#starttime").change(function() 
										{
											updateCost();
										});

										$("#endtime").change(function()
										{
											updateCost();
										});
									}else{
										$.notify({
											// options
											message: 'This parking spot is already reserved.' 
										},{
											// settings
											type: 'danger'
										});
									}
								},
								error: function(er)
								{

								}
							})
						});
					} else {
						$("#empty_search_results").fadeIn();
					}

				},
				error : function()
				{

				}
			})
		}



		var geocodeAddress = function(geocoder, resultsMap, spot) {
			geocoder.geocode({'address': spot["address"]}, function(results, status) {
				if (status === 'OK') {
					
					resultsMap.setCenter(results[0].geometry.location);

					var marker = new google.maps.Marker({
						map: resultsMap,
						icon: "/images/" + spot["status"],
						position: results[0].geometry.location
					});

				} else {
					console.log("Location could not be found")
				}
			});
		}
	</script>
	<script type="text/javascript" src="/assets/js/dashboard.js"></script>
	@endsection