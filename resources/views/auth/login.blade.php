
@extends("layouts.home")

@section('content')

@include ("partials.navigation")

<div class="background-container"></div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card auth-card-margin">
                <div class="card-header auth-card-header">Login</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="center">
                            <div class="margin-top">
                                <label><input type="checkbox" style="margin-bottom:20px" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me (for 30 days)</label>
                                <br>
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="anchor" style="margin-left:20px" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
