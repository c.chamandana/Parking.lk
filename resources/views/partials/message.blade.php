@if (Session::get("message") && Session::get("message-type"))
	<script type="text/javascript">
		$.notify({
			// options
			message: '{{ Session::get("message") }}' 
		},{
			// settings
			type: '{{ Session::get("message-type") }}'
		});
	</script>
@endif