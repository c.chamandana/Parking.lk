
<nav class="navbar navbar-expand-md fixed-top " color-on-scroll="500">
	<div class="container">
		<div class="navbar-translate">
			<button class="navbar-toggler navbar-toggler-right navbar-burger" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-bar"></span>
				<span class="navbar-toggler-bar"></span>
				<span class="navbar-toggler-bar"></span>
			</button>
			<a class="navbar-brand" href="/">Parking.lk</a>
		</div>
		<div class="collapse navbar-collapse" id="navbarToggler">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a href="/about" class="nav-link"><i class="nc-icon nc-alert-circle-i"></i> About Us</a>
				</li>			
				<li class="nav-item">
					<a href="/contact" class="nav-link"><i class="nc-icon nc-email-85"></i> Contact Us</a>
				</li>

				@if(Auth::check())
					<li class="nav-item">
						<a href="/dashboard" class="nav-link"><i class="nc-icon nc-circle-10"></i> Dashboard</a>
					</li>
					<li class="nav-item">
						<a href="/logout" class="nav-link"><i class="nc-icon nc-circle-10"></i> Log out</a>
					</li>
				@else
					<li class="nav-item">
						<a href="/register" class="nav-link"><i class="nc-icon nc-circle-10"></i> Signup</a>
					</li>
					<li class="nav-item">
						<div class="dropdown">
							<a href="/login" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
								login
								<b class="caret"></b>
							</a>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<li class="dropdown-header">Select</li>
								<a class="dropdown-item" href="/login?type=driver">Driver</a>
								<a class="dropdown-item" href="/login?type=owner">Parking Owner</a>
								<a class="dropdown-item" href="/login?type=admin">Admin</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="/logout">Logout</a>
							</ul>
						</div>
					</li>
				@endif
			</ul>
		</div>
	</div>
</nav>