<script id="spot-template" type="text/x-handlebars-template">
	<div data-spotid="{{ spotid }}" class="spot-modal-trigger col col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="spot-card">
			<div class="location-panel">
				<table>
					<tr>
						<td><i class="fa fa-map-marker"></i></td>
						<td style="padding-left:25px;">{{ breaklines address }}</td>
					</tr>
				</table>
			</div>

			<div class="spot-state {{ statusLower }}">
				<i class="fa fa-circle"></i> {{ status }}
			</div>
		</div>
	</div>
</script>