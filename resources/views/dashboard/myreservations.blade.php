<div class="modal fade" id="cancel-reservation-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				Are you sure that you want to cancel the reservation
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
				<a id="cancel-yes-button" href="#"><button type="button" class="btn btn-primary">Yes</button></a>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col col-lg-12 col-md-12 col-sm-12">
		<div class="dashboard-section">
			<h4>SEARCH PARKING SPOTS</h4>

			<form id="search_query_form">
				<div class="input-group mb-3">
					<input id="search_query_txt" type="text" class="form-control" placeholder="Search for parking spots using location" aria-label="Recipient's username" aria-describedby="basic-addon2">
					<div class="input-group-append">
						<button id="search_query_btn" class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
					</div>
				</div>
			</form>

			<div id="search-result-container" class="row" style="margin-top:50px;">
				<?php $myreservations = \App\Reservation::where("driver", \Auth::user()->email)->get(); ?>

				@if(sizeof($myreservations) > 0)
				<table class="table">
					<thead class="table-header-white-text">
						<th>Approval</th>
						<th>Address</th>
						<th>Arrival Time</th>
						<th>Departure Time</th>
						<th>Estimated Cost</th>
						<th>Take Action</th>
					</thead>
					@foreach ($myreservations as $reservation)
					<?php 
						$spot = \App\Spot::where("id", $reservation->spotid)->first(); 
						$hourdiff = round((strtotime($reservation->end_time) - strtotime($reservation->start_time)) / 3600, 1); 
						$cost = $hourdiff * 100;
					?>

					<tr>
						@if ($reservation->approved)
							<td class="center" style="color:lightgreen"><i class="fa fa-check-circle"></i></td>
						@else
							<td class="center" style="color:salmon"><i class="fa fa-times-circle"></i></td>
						@endif
						<td style="color:salmon; font-weight:bold">{{ $spot->address1 . ", " . $spot->address2 . ", " . $spot->address3 }} </td>
						<td>{{ $reservation->start_time }}</td>
						<td>{{ $reservation->end_time }}</td>
						<td>{{ "LKR. " . $cost . ".00" }}</td>
						<td class="center"><a data-toggle="modal" data-target="#cancel-reservation-modal" class="cancel-reserve-button btn btn-primary" href="#myreservations" data-href="/cancel/reservation/{{ $reservation->id }}">Cancel Reservation</a></td>
					</tr>		
					@endforeach
				</table>
				@else
				<div id="empty_search_results">
					Did not find any reservations... <a href="#spots">Go reserve now</a>
				</div>
				@endif

			</div>

		</div>
	</div>
</div>