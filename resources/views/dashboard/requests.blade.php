<div class="row">
	<div class="col col-lg-12 col-md-12 col-sm-12">
		<div class="dashboard-section">
			<h4>PARKING RESERVATION REQUESTS</h4>

			<?php 
			$myspots = \App\Spot::where("owner", \Auth::user()->email)->get(); 
			$found = false;

			foreach ($myspots as $spot)
			{
				?>

				@if(\App\Reservation::where("spotid", $spot->id)->where("approved", 0)->exists())
					<?php $found = true; ?>
				<p style="font-weight:bold;color:salmon">{{ $spot->address1 . ", " . $spot->address2 . ", " . $spot->address3 }} <i class="fa fa-chevron-down"></i></p>
					<div class="request-card card" style="margin-bottom:25px">								

						<table class="td-centered" style="width:100%">
							<thead>
								<th colspan="2">
									Actions
								</th>
								<th>
									Driver Email
								</th>
								<th>
									Date Requested
								</th>
								<th>
									Estimated Income
								</th>
							</thead>
							@foreach (\App\Reservation::where("spotid", $spot->id)->where("approved", 0)->get() as $reservation)
							<tr>
								<td style="width:85px">
									<a href="/accept/reservation/{{ $reservation->id }}"><button class="btn btn-primary">ACCEPT</button></a>
								</td>
								<td style="width:85px">
									<a href="/cancel/reservation/{{ $reservation->id }}"><button class="btn btn-danger">REFUSE</button></a>
								</td>
								<td>
									{{ $reservation->driver }}
								</td>
								<td>
									{{ $reservation->created_at }}
								</td>
								<td>
									<?php 
									$hourdiff = round((strtotime($reservation->end_time) - strtotime($reservation->start_time)) / 3600, 1); 
									$cost = $hourdiff * 100;
									?>

									LKR. {{$cost}}.00
								</td>
							</tr>
							@endforeach
						</table>
					</div>
				@endif
				<?php
			}
			?>

			@if ($found == false)
				<div class="padded">
					No parking reservations found for your parking spots...
				</div>
			@endif
		</div>
	</div>
</div>