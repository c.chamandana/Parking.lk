<div class="row justify-content-md-center" style="margin-top:25px">
	<div class="col col-lg-6 col-md-6 col-sm-12">
		<h4 style="color:salmon">Profile Settings</h4>

		<form enctype="multipart/form-data" action="/save/settings" method="POST" style="margin-top:25px">
			{{ csrf_field() }}
			<label for="name">Change your name</label>
			<input required type="text" id="name" name="name" value="{{ \Auth::user()->name }}" placeholder="ex: John Doe" class="form-control">

			<button style="margin-top:20px" type="submit" class="btn btn-danger">Save Settings</button>
		</form>

		<h4 style="margin-top:50px;color:salmon">Change your Password</h4>
		<form enctype="multipart/form-data" action="/change/password" method="POST" style="margin-top:25px">
			{{ csrf_field() }}
			<label for="password">Current Password</label>
			<input class="form-control" type="password" id="password" name="password" placeholder="Enter your existing password">

			<label style="margin-top:15px;" for="confirm-password">Confirm Password</label>
			<input class="form-control" type="password" id="confirm-password" name="confirm-password" placeholder="Enter your existing password">

			<button style="margin-top:20px" type="submit" class="btn btn-danger">Change Password</button>
		</form>

		<h4 style="margin-top:50px;color:salmon">Account Settings</h4>

		<table class="tr-spacer" style="margin-top:25px; width:100%"> 
			<tr>
				<td>
					<strong>Delete Account</strong><br>
					<span>Delete your Parking.lk account permanently</span>
				</td>
				<td>
					<a href="/delete/account" class="btn btn-danger">Delete Account</a>
				</td>
			</tr>

			@if (\Auth::user()->type == 0)
			<tr>
				<td>
					<strong>Switch Account Type</strong><br>
					<span>Switch your account type to a <strong>Parking Spot Owner</strong></span>
				</td>
				<td>
					<a href="/switch/account" class="btn btn-danger">Switch Account</a>
				</td>
			</tr>
			@elseif (\Auth::user()->type == 1)
			<tr>
				<td>
					<strong>Switch Account Type</strong><br>
					<span>Switch your account type to a <strong>Driver</strong></span>
				</td>
				<td>
					<a href="/switch/account" class="btn btn-danger">Switch Account</a>
				</td>
			</tr>
			@endif
		</table>
	</div>
</div>