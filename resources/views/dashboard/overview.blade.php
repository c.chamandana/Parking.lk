<div class="row" style="margin-top:75px">
	<div class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<h4 style="text-align:center">User Accounts Composition</h4>

		<div class="chart-container" style="position: relative; height:400px !important; width:100%">
			<canvas id="user-accounts-chart" width="100%" height="400px">
			</canvas>
		</div>
	</div>

	<div class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<h4 style="text-align:center">Accounts created in this month</h4>

		<div class="chart-container" style="position: relative; height:400px !important; width:100%">
			<canvas id="month-accounts" width="100%" height="400px">
			</canvas>
		</div>
	</div>
</div>


<div class="row" style="margin-top:100px;">
	<div class="col col-lg-12">
		<h4 style="text-align:center;display:block;margin-bottom:50px;">Map of available Parking Spots</h4>

		<div id="map" style="height: 600px; width: 100%;">

		</div>
	</div>
</div>