<div class="modal fade" id="reserve-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Reserve Parking Spot</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<iframe
					id="google-maps-frame"
					width="100%"
					height="300"
					style="margin-bottom:20px"
					frameborder="0" style="border:0"
					src="" allowfullscreen>
				</iframe>

				<form id="reserve-spot-form" action="/reserve-spot" enctype="multipart/form-data" method="POST">
					{{ csrf_field() }}
					<input type="hidden" name="spotid" id="spotid">

					<label for="address">Parking Address</label>
					<input type="text" id="address" readonly class="form-control" value="">

					<label for="address" style="margin-top:10px">Parking Spot Owner</label>
					<input type="text" id="owner" readonly class="form-control" value="">

					<label for="address" style="margin-top:10px;display:block">Arriving Date and Time</label>
					<div class='input-group date' style="margin-top:10px">
			            <input name="starttime" validate required id="starttime" type='text' class="form-control" placeholder="The Date and time you're going to park on the location"/>
			            <span class="input-group-addon">
			                <span class="fa fa-calendar">
			                </span>
			            </span>
			        </div>

					<label for="address" style="margin-top:10px;display:block">Date and Time of Departure</label>
					<div class='input-group date' style="margin-top:10px">
			            <input name="endtime" validate required id="endtime" type='text' class="form-control" placeholder="The Date and time you're going to park on the location"/>
			            <span class="input-group-addon">
			                <span class="fa fa-calendar">
			                </span>
			            </span>
			        </div>

			        <label style="margin-top:10px;" for="estimated-cost">Estimated Cost</label>
			        <input id="estimated-cost" readonly class="form-control" type="text" placeholder="Please select a duration to calculate cost">
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<button id="request-reserve-btn" type="submit" class="btn btn-primary">Request Parking Spot</button>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col col-lg-12 col-md-12 col-sm-12">
		<div class="dashboard-section">
			<h4>SEARCH PARKING SPOTS</h4>

			<form id="search_query_form">
				<div class="input-group mb-3">
					<input id="search_query_txt" type="text" class="form-control" placeholder="Search for parking spots using location" aria-label="Recipient's username" aria-describedby="basic-addon2">
					<div class="input-group-append">
						<button id="search_query_btn" class="btn btn-outline-secondary" type="submit"><i class="fa fa-search"></i></button>
					</div>
				</div>
			</form>

			<div id="search-result-container" class="row" style="margin-top:50px;">
				{{-- Keep this place empty as the content may be deleted on runtime --}}
			</div>

			<div id="empty_search_results">
				No results found for your query
			</div>

		</div>
	</div>
</div>

@include("dashboard.spot-card")