<div class="row">
	<div class="col col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h4>Driver Accounts</h4>

		<div class="table-container">
			<table id="drivers-table" class="table table-striped display" cellspacing="0" width="100%">
				<thead>
		            <tr>
		                <th>Name</th>
		                <th>Email</th>
		                <th>Actions</th>
		            </tr>
		        </thead>
		        <tbody>
		        	@foreach (\App\User::where("type", 0)->get() as $user)
			        	<tr>
			        		<td>{{ $user->name }}</td>
			        		<td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
			        		<td style="width:132px">
			        			<button class="btn btn-primary">Delete Account</button>
			        		</td>
			        	</tr>
		        	@endforeach
		        </tbody>
			</table>
		</div>
	</div>

	<div class="col col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h4>Park Owners</h4>

		<div class="table-container">
			<table id="owners-table" class="table table-striped display" cellspacing="0" width="100%">
				<thead>
		            <tr>
		                <th>Name</th>
		                <th>Email</th>
		                <th>Actions</th>
		            </tr>
		        </thead>
		        <tbody>
		        	@foreach (\App\User::where("type", 1)->get() as $user)
			        	<tr>
			        		<td>{{ $user->name }}</td>
			        		<td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
			        		<td style="width:132px">
			        			<a href="/delete/account/{{ $user->id }}"><button class="btn btn-primary">Delete Account</button></a>
			        		</td>
			        	</tr>
		        	@endforeach
		        </tbody>
			</table>
		</div>
	</div>
</div>