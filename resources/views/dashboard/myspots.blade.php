<!-- Create Modal -->
<div class="modal fade" id="add-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form enctype="multipart/form-data" action="/create/spot" method="POST">
				{{ csrf_field() }}
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Add a new Parking Spot</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<label for="address1">Address of the Vehicle Park</label>
					<input style="margin-top:5px" type="text" class="form-control" id="add-address1" name="address1" placeholder="Address Line 1">
					<input style="margin-top:5px" type="text" class="form-control" id="add-address2" name="address2" placeholder="Address Line 2">
					<input style="margin-top:5px" type="text" class="form-control" id="add-address3" name="address3" placeholder="Address Line 3">
				</div>
				<div class="modal-footer">
					<button id="add-park-button" type="submit" class="btn btn-primary">Add Park</button>
					<button data-dismiss="modal" type="submit" class="btn btn-secondary">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Edit Modal -->
<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="edit-spot-form" enctype="multipart/form-data" action="/save/spot/{X}" method="POST">
				{{ csrf_field() }}
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Edit Parking Spot Details</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<label for="address1">Address of the Vehicle Park</label>
					<input style="margin-top:5px" type="text" class="form-control" id="edit-address1" name="address1" placeholder="Address Line 1">
					<input style="margin-top:5px" type="text" class="form-control" id="edit-address2" name="address2" placeholder="Address Line 2">
					<input style="margin-top:5px" type="text" class="form-control" id="edit-address3" name="address3" placeholder="Address Line 3">
				</div>
				<div class="modal-footer">
					<button id="save-park-button" type="submit" class="btn btn-primary">Update Park</button>
					<button data-dismiss="modal" type="submit" class="btn btn-secondary">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="row">
	<div class="col col-lg-12 col-md-12 col-sm-12">
		<div class="dashboard-section">
			<h4>MY PARKING SPOTS</h4>

			<div class="cards-container">
				<div class="row">

					<?php $myspots = \App\Spot::where("owner", \Auth::user()->email)->get(); ?>

					@foreach ($myspots as $spot)
					<div data-spotid="{{ $spot->id }}" class="spot-click-trigger col col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div class="spot-card">
							<div class="location-panel">
								<table>
									<tr>
										<td><i class="fa fa-map-marker"></i></td>
										<td style="padding-left:25px;">{{ $spot->address1 }},<br>{{ $spot->address2 }},<br>{{ $spot->address3 }}</td>
									</tr>
								</table>
							</div>

							<div class="spot-state {{strtolower(\App\Http\Controllers\AJAXController::checkSpotStatus($spot->id)) }}">
								<i class="fa fa-circle"></i> {{ strtoupper(\App\Http\Controllers\AJAXController::checkSpotStatus($spot->id)) }}
							</div>
						</div>
					</div>
					@endforeach

					<div class="col col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<div id="add-spot-trigger" class="add-card">
							<div class="middled-content">
								<i class="fa fa-plus"></i> Add new parking spot
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="dashboard-section" style="margin-top:50px;">
			<h4>PARKING SPOTS MAP</h4>
			<div id="map" style="height: 600px; width: 100%;">
			</div>
		</div>
	</div>