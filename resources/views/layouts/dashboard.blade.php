<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="/assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>{{ Auth::user()->name }} | Dashboard | Parking.lk</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />

	<link href="/assets/css/bootstrap.min.css" rel="stylesheet" />

	<link rel="stylesheet" type="text/css" href="/css/main.css">

	<!--     Fonts and icons     -->
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/css/nucleo-icons.css" rel="stylesheet" />

	{{-- Additional Dependencies --}}
	<link rel="stylesheet" type="text/css" href="/bower_components/ccrouter/ccrouter.css">
	<link rel="stylesheet" type="text/css" href="/bower_components/dynatable/jquery.dynatable.css">
	<link rel="stylesheet" type="text/css" href="/bower_components/animate.css/animate.css">
	<link rel="stylesheet" type="text/css" href="/bower_components/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css">
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
	
</head>
<body>
	@yield ("content")

	<!-- Core JS Files -->
	<script src="/assets/js/jquery-3.2.1.js" type="text/javascript"></script>
	<script src="/assets/js/jquery-ui-1.12.1.custom.min.js" type="text/javascript"></script>
	<script src="/assets/js/popper.js" type="text/javascript"></script>
	<script src="/assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Plugins for DateTimePicker -->
	<script src="/assets/js/moment.min.js"></script>

	<!-- Switches -->
	<script src="/assets/js/bootstrap-switch.min.js"></script>

	<!--  Plugins for Slider -->
	<script src="/assets/js/nouislider.js"></script>


	<!--  Paper Kit Initialization snd functons -->
	<script src="/assets/js/paper-kit.js?v=2.1.0"></script>

	<script type="text/javascript" src="/bower_components/ccrouter/ccrouter.js"></script>

	<script type="text/javascript" src="/bower_components/dynatable/jquery.dynatable.js"></script>
	<script type="text/javascript" src="/bower_components/handlebars/handlebars.min.js"></script>

	<script type="text/javascript" src="/bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>

	<script type="text/javascript" src="/bower_components/bootstrap-material-design/dist/js/material.min.js"></script>
	<script type="text/javascript" src="/bower_components/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

	<script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

	<script type="text/javascript" src="/bower_components/chart.js/dist/Chart.min.js"></script>
	
	@include("partials.message")

	@yield("scripts")
</body>
</html>