@extends ("layouts.home")

@section ("content")
	<!-- Navigation bar -->

	@include ("partials.navigation")

	<!-- end navbar  -->


	<div class="wrapper">

		<div class="Search_Window_Main">

			<!-- login search starts here  -->
			<div class="row">
				<div class="col-md-4 searchbar-container">
					<div class="card card-register">

						<h3 class="title">Find hassle free <br>parking in seconds</h3>

						<form class="register-form" action="/search" enctype="multipart/form-data">
							{{ csrf_field() }}

							<label>Where to park? </label>

							<div class="form-group">
								<input required type="text" name="address" class="form-control" placeholder="Enter your location">
							</div>

							<label>Arriving</label>

							<div class='input-group date' id='datetimepicker'>
								<input required type='text' name="starttime" class="form-control datetimepicker" placeholder="Select date and time" />
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"><i class="fa fa-calendar" aria-hidden="true"></i></span>
								</span>
							</div>

							<label>Departuring</label>

							<div class='input-group date' id='datetimepicker'>
								<input required type='text' name="endtime" class="form-control datetimepicker" placeholder="Select date and time" />
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"><i class="fa fa-calendar" aria-hidden="true"></i></span>
								</span>
							</div>

							<button type="submit" class="btn btn-danger btn-block btn-round">Find a Spot</button>
						</form>
						<div class="forgot">
							<a href="/register" class="btn btn-link btn-danger">Learn more?</a>
						</div>
					</div>
				</div>


				<div class="col-md-6">

					<div class="card page-carousel">
						<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
							</ol>
							<div class="carousel-inner" role="listbox">
								<div class="carousel-item active">
									<img class="d-block img-fluid" src="https://www.driving.co.uk/s3/st-driving-prod/uploads/2016/12/angled-bays.jpg" alt="First slide">
									<div class="carousel-caption d-none d-md-block">
									</div>
								</div>
								<div class="carousel-item">
									<img class="d-block img-fluid" src="https://images.techhive.com/images/article/2015/09/cars_parked_underground_garage-100616290-orig.jpg" alt="Second slide">
									<div class="carousel-caption d-none d-md-block">
									</div>
								</div>
								<div class="carousel-item">
									<img class="d-block img-fluid" src="http://www.vhe.co.uk/admin/resources/2renaissance-house-pr-car-park-1.jpg" alt="Third slide">
									<div class="carousel-caption d-none d-md-block">
									</div>
								</div>
							</div>

							<a class="left carousel-control carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
								<span class="fa fa-angle-left"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
								<span class="fa fa-angle-right"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>

				</div>
			</div>
			<div class="col-md-2">

			</div>
		</div>
	</div>
</div>

<div class="main">
	<div class="section text-center">
		<div class="container">
			<div class="row">
				<div class="col-md-8 ml-auto mr-auto">
					<h2 class="title">Why Parking.lk?</h2>
					<h5 class="description">Parking.lk is the newest solution to your biggest problem in parking. You don't need to worry about parking because with parking.lk, your parking spot is waiting for you when you arrive at the destination.</h5>
					<br>
					<a href="/register" class="btn btn-danger btn-round">Join now</a>
				</div>
			</div>
			<br><br>
			<div class="row">
				<div class="col-md-4">
					<div class="info">
						<div class="icon icon-danger">
							<i class="nc-icon nc-bulb-63"></i>
						</div>
						<div class="description">
							<h4 class="info-title">New Idea</h4>
							<p>We did not just create a product, we solved a big problem.</p>
							
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="info">
						<div class="icon icon-danger">
							<i class="nc-icon nc-chart-bar-32"></i>
						</div>
						<div class="description">
							<h4 class="info-title">Statistics for parking owners</h4>
							<p>Park owners can view their stats live with clear data representation</p>
							
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="info">
						<div class="icon icon-danger">
							<i class="nc-icon nc-sun-fog-29"></i>
						</div>
						<div class="description">
							<h4 class="info-title">Delightful design</h4>
							<p>Simple and minimal design to get your job done without any hassle</p>

						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="section section-dark text-center">
		<div class="container">
			<h2 class="title">Signup with Parking.lk and save your valuable time</h2>
			<div class="row">

			</div>
		</div>
	</div>


</div>

<!-- Modal Bodies come here -->

<!--   end modal -->

<!-- Footer Starts here -->

@include ("partials.footer")

@endsection
