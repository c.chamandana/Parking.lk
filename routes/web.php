<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\LoginInsist;

Route::get('/', function () {
    return view('index');
});

Route::post('/search', "SearchController@search");

Route::get("/dashboard", "DashboardController@view")->middleware(LoginInsist::class);

// Add the authentication routes
Auth::routes();

Route::get("/logout", "LogoutController@logout")->name("logout");

Route::get("/dashboard/tab/{tab}", "DashboardController@loadTab")->middleware(LoginInsist::class);

Route::get("/home", function()
{
	return redirect("/dashboard");
});

// Routes for AJAX Requests
Route::get("/ajax/spots/", "AJAXController@spotSearch");
Route::get("/ajax/spots/{query}", "AJAXController@spotSearch");
Route::get("/ajax/spot-details/{spotid}", "AJAXController@getSpotDetails");

// Reservation of spots
Route::post("/reserve-spot", "ReservationsController@reserve")->middleware(LoginInsist::class);
Route::get("/cancel/reservation/{reservationid}", "ReservationsController@cancel")->middleware(LoginInsist::class);
Route::get("/accept/reservation/{reservationid}", "ReservationsController@accept")->middleware(LoginInsist::class);

// Creating and updating Spots
Route::post("/create/spot", "SpotController@create")->middleware(LoginInsist::class);
Route::post("/save/spot/{spotid}", "SpotController@save")->middleware(LoginInsist::class);

// Account Management and Settings
Route::get("/delete/account/{userid}", "AccountController@delete")->middleware(LoginInsist::class);

Route::get("/search", function() { return view("search"); });

Route::get("/terms", function() { return view("terms"); });

Route::get("/about", function() { return view("about"); });
Route::get("/contact", function() { return view("contact"); });


// Settings routes
Route::post("/save/settings", "SettingsController@basic")->middleware(LoginInsist::class);
Route::post("/change/password", "SettingsController@changePassword")->middleware(LoginInsist::class);
Route::get("/delete/account", "SettingsController@deleteAccount")->middleware(LoginInsist::class);
Route::get("/switch/account", "SettingsController@switchAccount")->middleware(LoginInsist::class);